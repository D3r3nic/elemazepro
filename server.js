var express = require('express'); 
var app = express();
var bodyParser = require('body-parser');
// var mongoose= require('mongoose');

app.use(bodyParser.json());
//connects to the static folder of DIST
app.use(express.static( __dirname + '/public/dist/public' ));

//connects to the database
// mongoose.connect('mongodb://localhost/elemaze_test1');

//reference to mongoose.js
// require('./server/config/mongoose.js');

//reference to require.js
var routes=require('./server/config/routes.js');
routes(app);

app.listen(8000, function() {
    console.log("listening on port 8000");
})