"use strict";
const nodemailer = require("nodemailer");
// var mongoose = require('mongoose');


module.exports = {
	contactUs:function (req, res) {
		main()

		async function main(){
			const output = 
				`<p>You have a new contact request</p>
				<h3>Contact Details</h3>
				<ul>  
				<li>Name: ${req.body.name}</li>
				<li>Email: ${req.body.email}</li>
				<li>Phone: ${req.body.phone}</li>
				</ul>
				<h3>Message</h3>
				<p>${req.body.message}</p>`;

			let transporter = nodemailer.createTransport({
				host: "smtp.gmail.com",
				port: 587,
				secure: false, 
				auth: {
					user: 'XXX',
      				pass: 'XXX'
				},
				tls:{
					rejectUnauthorized:false
				}
				});
				
				let info = await transporter.sendMail({
					from: '"Elemazepros.com" <d3r3inc@gmail.com>', 
					to: "d3r3nic@gmail.com", 
					subject: "Contact us @ Elemaze",
					text: "Hello There! You have a message from Elemazepros.com",
					html: output
				});
			
			console.log("Message sent: %s", info.messageId);
			console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
			res.json({message:"success"})
		}
		main().catch(console.error);
	},
}