import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{

  constructor(private _router:Router){}

  ngOnInit() {
    this._router.navigate(['/welcome']);
  }

  navFb(){
    window.open("https://www.facebook.com/Elemaze-Production-2126399750762498/", "_blank");
  }

  navInst(){
    window.open("https://www.instagram.com/elemazeproduction/","_blank")
  }
}
