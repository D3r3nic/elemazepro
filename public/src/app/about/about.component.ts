import { ViewportScroller } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(viewportScroller: ViewportScroller) {
    viewportScroller.scrollToPosition([0,0])
  }
  ngOnInit() {
  }

}
