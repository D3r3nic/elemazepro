import { Component, OnInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  storage=window.localStorage

  
  constructor(viewportScroller: ViewportScroller) {
    viewportScroller.scrollToPosition([0,0])
  }
  ngOnInit() {
  // this.storage.getItem('logged')!="1"
  // this.storage.setItem("page","1");
    
  }
  goToWedding() {
    this.storage.setItem("photos","wedding");
  }
  goToEngage() {
    this.storage.setItem("photos","engage");
  }
  goToPreShoot() {
    this.storage.setItem("photos","preshoot");
  }
  goToBaptism() {
    this.storage.setItem("photos","baptism");
  }

}
