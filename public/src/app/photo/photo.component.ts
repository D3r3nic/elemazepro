import { Component, OnInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {
storage=window.localStorage
wedding1="w1.jpg";
engage1="e1.jpg"
preshoot1="p1.jpg";
baptism1="b1.jpg";
wedding=["w2.jpg","w3.jpg"];
engage=["e2.jpg","e3.jpg","e4.jpg"];
preshoot=["p2.jpg","p3.jpg","p4.jpg"];
baptism=["b2.jpg","b3.jpg"];
page=1;
freshpage=true;
constructor(viewportScroller: ViewportScroller) {
  viewportScroller.scrollToPosition([0,0])
  }
  
  ngOnInit() {
    console.log(this.storage.getItem('photos'))
    if(this.storage.getItem('photos') && this.freshpage){
      this.freshpage=false;

      if(this.storage.getItem('photos')=="wedding"){
        this.weddingPhotos()
      }
      if(this.storage.getItem('photos')=="engage"){
        this.engagePhotos()
      }
      if(this.storage.getItem('photos')=="preshoot"){
        this.prePhotos()
      }
      if(this.storage.getItem('photos')=="baptism"){
        this.bapPhotos()
      }
      this.storage.clear();
    }
    if(this.freshpage){
      this.freshpage=false;
      this.weddingPhotos()
    }
    return
    

  }

  weddingPhotos(){
    this.page=1
  }
  engagePhotos(){
    this.page=2
  }
  prePhotos(){
    this.page=3
  }
  bapPhotos(){
    this.page=4
  }

}
