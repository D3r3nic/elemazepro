import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AppComponent } from './app.component';
import { PhotoComponent } from './photo/photo.component';
import { AboutComponent } from './about/about.component';
import { VideoComponent } from './video/video.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  {path:'', pathMatch:'full', component:AppComponent},

  {path:'welcome',component:WelcomeComponent},
  {path:'home',component:HomeComponent},
  {path:'photogallery',component:PhotoComponent},
  {path:'videos',component:VideoComponent},
  {path:'about',component:AboutComponent},
  {path:'contactus',component:ContactUsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
