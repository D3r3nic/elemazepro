import { Component, OnInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
  
})
export class ContactUsComponent implements OnInit {
  user={name:"",phone:"",email:"",message:""}
  message=""

  constructor(private _httpService:HttpService ,viewportScroller: ViewportScroller, private _router:Router) {
    viewportScroller.scrollToPosition([0,0])
  }

  ngOnInit() {
  }
  

  contacUs(){
    let tempObservable=this._httpService.contactUs(this.user);
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        console.log(data)
        // this._router.navigate(['/contactus'])
        this.message="Message has been succesfully sent"
      }else{
        this.message=data.err
      }
    })
    console.log(this.user)
  }
}
