
$( document ).ready(function() {

    $(document).on('click','.photoMenu li',function(){
        $(this).addClass('active').siblings().removeClass('active')
    })
    
    $(document).on('click','#wedding',function(){
        $('#weddingButton').addClass('active').siblings().removeClass('active')
    })
    $(document).on('click','#engage',function(){
        $('#engButton').addClass('active').siblings().removeClass('active')
    })
    $(document).on('click','#pre',function(){
        $('#preButton').addClass('active').siblings().removeClass('active')
    })
    $(document).on('click','#bapt',function(){
        $('#baptButton').addClass('active').siblings().removeClass('active')
    })

    $(document).on('click','.routerLink',function(){
        $("#drawer-toggle-label").click()
    })
});